use Test;

use Gemini::Response;
use Gemini::Server;

use IO::Socket::Async::SSL;

my constant HOST = '127.0.0.1';
my constant PORT = 52301;

sub mk-server {
    Gemini::Server.new(
        HOST, PORT,
        certificate-file => 't/cert/certificate.crt',
        private-key-file => 't/cert/certificate.key')
}

sub mk-client {
    IO::Socket::Async::SSL.connect(HOST, PORT, :insecure)
}

sub do-react(&handler, &test, $desc) {
    react {
        whenever mk-server() { &handler($_) }
        whenever start {
            subtest &test, $desc;
        } { done }
    }
}

my @chans = [Channel.new, Channel.new];

do-react -> ($req, $vow) {
    start {
        my $chan = @chans[+$req.url - 1];

        $vow.keep: Gemini::Response.new(body => $req.url);

        $chan.receive;
    };
}, {
    my $sock1 = await mk-client;
    my $sock2 = await mk-client;

    my $res;
    my ($chan1, $chan2) = @chans;

    $sock1.print: "1\r";
    $sock2.print: "2\r\n";

    $res = Gemini::Response.parse: [~] $sock2.Supply(:bin).Seq;
    cmp-ok $res, '~~', Gemini::Response.new(body => '2');

    $sock1.print: "\n";

    $res = Gemini::Response.parse: [~] $sock1.Supply(:bin).Seq;
    cmp-ok $res, '~~', Gemini::Response.new(body => '1');

    $chan1.send: Nil;
    $chan2.send: Nil;
}, 'Async';

do-react -> ($req, $vow) {
    @chans[0].send: Nil;
    @chans[1].receive;
    $vow.keep: Gemini::Response.new;
    @chans[0].send: Nil;
}, {
    my $sock1 = await mk-client;
    $sock1.print: "a\r\n";
    @chans[0].receive;
    $sock1.close;
    @chans[1].send: Nil;
    @chans[0].receive;
}, 'Closed by client';

do-react -> ($req, $vow) {
    $vow.keep: Gemini::Response.new;
}, {
    my $sock1 = await mk-client;
    $sock1.write: blob8.new: <192 32 13 10>;
    $sock1.Supply.wait;
}, 'Invalid UTF-8';

@chans.map: { .close };

done-testing;
