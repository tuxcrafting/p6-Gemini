use Test;

use Gemini::Response;

sub is-response($s, $a, *%h) {
    cmp-ok $a, '~~', Gemini::Response.partial(|%h), $s;
}
sub isnt-response($s, $a, *%h) {
    cmp-ok $a, { not $^a ~~ $^b }, Gemini::Response.partial(|%h), $s;
}

is-response(
    'Creation',
    Gemini::Response.new(SUCCESS, 'text/gemini', 'Hello, World!'),
    status => 20,
    meta => 'text/gemini',
    body => 'Hello, World!');

is-response(
    'Body other encoding',
    Gemini::Response.new(body => 'é', enc => 'latin-1'),
    body => blob8.new(233));

is-response(
    'Body blob8',
    Gemini::Response.new(body => blob8.new(233)),
    body => blob8.new(233));

is-response(
    'Parse',
    Gemini::Response.parse("20 meta\r\nbody".encode),
    status => 20,
    meta => 'meta',
    body => 'body');

isnt-response(
    'Not accept pattern',
    Gemini::Response.new,
    status => 21);

is-response(
    'Accept pattern',
    Gemini::Response.partial,
    status => 21);

is-response(
    'Parse empty meta',
    Gemini::Response.parse("20 \r\nbody".encode),
    status => 20,
    meta => '',
    body => 'body');

dies-ok { Gemini::Response.parse("20 meta\nbody"); },
'Parse fail no header';

dies-ok { Gemini::Response.parse("0 aa\r\nhello"); },
'Parse fail invalid status';

dies-ok { Gemini::Response.parse("77 aa\r\nhello"); },
'Parse fail out-of-range status';

dies-ok { Gemini::Response.parse("20\r\nhello"); },
'Parse fail no meta';

done-testing;
