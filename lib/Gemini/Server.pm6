use IO::Socket::Async::SSL;

#| Gemini request object.
class Gemini::Server::Request {
    #| The URL. Gemini::Server does no sanitization to allow for
    #| maximum genericity, but you should normally check whether it's
    #| actually a gemini:// URL (or other depending on your usecase).
    has Str:D $.url is required;
    #| Peer host.
    has Str:D $.host is required;
    #| Peer port.
    has Int:D $.port is required;
}

#| Gemini server.
class Gemini::Server {
    multi method new(::?CLASS:U: Supply:D() $sock --> Supply:D) {
        supply {
            my $emit-chan = Channel.new;
            whenever $emit-chan { .emit }

            whenever $sock -> $conn {
                whenever $conn.Supply(:bin).produce(&infix:<~>) {
                    if .decode('utf8-c8') ~~ rx/(.+?) "\r\n"/ {
                        my $req = Gemini::Server::Request.new(
                            url => $0.Str,
                            host => $conn.peer-host,
                            port => $conn.peer-port);

                        my $promise = Promise.new;

                        start {
                            my $res = await $promise;
                            await $conn.write($res.blob);
                            $conn.close;
                        };

                        $emit-chan.send: ($req, $promise.vow);
                    }
                }
            }

            CLOSE {
                $emit-chan.close;
            }
        }
    }

    #| Create a server. Return a supply of lists containing a
    #| Gemini::Server::Request and a Vow, which should be kept with a
    #| Gemini::Response.
    multi method new(
        ::?CLASS:U: Str:D() $host, Int:D() $port = 1965,
        |c --> Supply:D) {
        self.new: IO::Socket::Async::SSL.listen($host, $port, |c);
    }
}
