class X::Gemini::MalformedResponse is Exception {
    method message {
        "Malformed response"
    }
}

#| Status code.
subset Gemini::StatusCode of Int where * ~~ 10..59;

enum Gemini::StatusCodeEnum (
    INPUT => 10,
    SENSITIVE-INPUT => 11,
    SUCCESS => 20,
    REDIRECT-TEMPORARY => 30,
    REDIRECT-PERMANENT => 31,
    TEMPORARY-FAILURE => 40,
    SERVER-UNAVAILABLE => 41,
    CGI-ERROR => 42,
    PROXY-ERROR => 43,
    SLOW-DOWN => 44,
    PERMANENT-FAILURE => 50,
    NOT-FOUND => 51,
    GONE => 52,
    PROXY-REQUEST-REFUSED => 53,
    BAD-REQUEST => 59,
    CLIENT-CERTIFICATE-REQUIRED => 60,
    CERTIFICATE-NOT-AUTHORISED => 61,
    CERTIFICATE-NOT-VALID => 62
);

#| Gemini response object.
class Gemini::Response {
    #| Status code. Refer to the Gemini specification for more
    #| information.
    has Gemini::StatusCode $.status is rw;
    #| Meta string. Interpretation is status code-dependent.
    has Str $.meta is rw;
    #| The actual response body.
    has blob8 $.body is rw;

    multi method new(
        ::?CLASS:U: :$status = SUCCESS,
        :$meta = '', :$body = '', :$enc = 'utf8') {

        self.partial: :$status, :$meta, :$body, :$enc
    }
    multi method new(::?CLASS:U: *@a) {
        self.new: |%(flat <status meta body enc> Z @a)
    }

    my proto sub get-body($body, Str:D $enc) {*}
    my multi sub get-body(blob8 $body, $enc) {
        return $body;
    }
    my multi sub get-body(Str:D() $body, $enc) {
        return $body.encode($enc);
    }
    my multi sub get-body(Any:U $body, $enc) {
        return blob8;
    }

    method partial(
        ::?CLASS:U:
        Gemini::StatusCode :$status = Gemini::StatusCode,
        Str :$meta,
        :$body, Str:D() :$enc = 'utf8') {

        self.bless: :$status, :$meta, body => get-body($body, $enc)
    }

    #| Parse a Gemini response.
    method parse(::?CLASS:U: blob8:D $data --> Gemini::Response:D) {
        my Int $header-last = Int;
        for $data[1..*]:kv -> $i, $c {
            if $data[$i - 1] == 13 and $c == 10 {
                $header-last = $i;
                last;
            }
        }

        X::Gemini::MalformedResponse.new.throw without $header-last;

        $data.subbuf(0..$header-last).decode ~~ rx/^(\d\d) ' ' (.*?) "\r\n"$/;
        X::Gemini::MalformedResponse.new.throw unless $/;

        my $status = $0.Int;
        my $meta = $1.Str;

        my $body = blob8.new: $data[$header-last + 1..*];

        self.new: :$status, :$meta, :$body
    }

    #| Serialize a response.
    method blob(::?CLASS:D: --> blob8:D) {
        "{$!status.Int} $!meta\r\n".encode ~ $!body
    }

    sub ACCEPTS-defined($a, $b) {
        do with $a & $b { $a ~~ $b } else { True }
    }

    method ACCEPTS(::?CLASS:D: Gemini::Response:D $other --> Bool:D) {
        [and] (
            ($!status, $other.status),
            ($!meta, $other.meta),
            ($!body, $other.body)).map({ ACCEPTS-defined |$_ });
    }
}
